#  Web Voto Online

## Sobre

Projeto desenvolvido na disciplina Resolução de Problemas VI. O projeto Web Voto Online se trata de um sistema de votação online à ser integrado no Guri, sistema existente da Unipampa. O Web Voto Online tem como objetivo prover uma forma fácil, clara e segura de votação em questões pertinentes a universidade. O sistema foi desenvolvido em PHP, HTML5, JavaScript e MySql. A escolha da tecnologia de desenvolvimento se deu devido ao fato da necessidade do sistema ser acessível em diversas plataformas por diversos sistemas operacionais.


## Autores

Filipe Garcia
Giliardi Schmidt
Guilherme Bolfe
Victor Ribeiro

## Requisitos

-PHP 7.2.10
-MySQL 5.7.23
-Apache 2.4.35

## Instalação

Primeiramente faça o download do arquivo do projeto no link https://bitbucket.org/NaiharaSAmorim/rpvi_gerencia_grupo02/src/master/ , copie os arquivos do download para seu servidor, renomeie os arquivos rpvi_gerencia_grupo02\webVotoCodeIgniter\application\config\sample.config.php e rpvi_gerencia_grupo02\webVotoCodeIgniter\application\config\sample.database.php de forma que o "sample." seja removido e os arquivos fiquem com os nomes config.php e database.php, logo após configure os arquivos como arquivos de configuração padrão do CodeIgniter onde devem ser informadas as informações sobre o banco como usuário, database, senha, driver entre outros; já no outro arquivo config.php deve ser configurado a url base. Após os passos exporte o arquivo \rpvi_gerencia_grupo02\Projeto\banco_final.sql para o banco. Após isso a instalação foi concluída. Mais detalhes entrar em contato com o suporte em vitao375@hotmail.com ou vitao375@gmail.com.


