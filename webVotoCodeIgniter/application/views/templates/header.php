<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta property="og:url" content="http://www.webvotoonline.com"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content=" "/>
    <meta property="og:author" content="Grupo 2"/>
    <meta property="og:description" content="web voto online"/>
    <meta property="og:image" content="http://www.victorribeiro.com/post.jpg"/>
    <title> Web Voto - Voto Online </title>
    <link rel="icon" href="<?= base_url('assets/img/favicon-guri16.png'); ?>" type="image/png"/>
    <link rel="shortcut icon" href="<?= base_url('assets/img/favicon-guri16.png'); ?>" type="image/png"/>
    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" href="<?= base_url('assets/css/main.css'); ?>"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url('assets/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.css') ?>">
    <link rel="stylesheet"
          href="<?= base_url('assets/DataTables/RowReorder-1.2.4/css/rowReorder.bootstrap.min.css') ?>">
    <script src="<?= base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/DataTables/DataTables-1.10.18/js/jquery.dataTables.js'); ?>"></script>
    <script src="<?= base_url('assets/DataTables/RowReorder-1.2.4/js/dataTables.rowReorder.min.js'); ?>"></script>
    <script src="<?= base_url('assets/DataTables/DataTables-1.10.18/js/dataTables.bootstrap4.js'); ?>"></script>
    <script src="<?= base_url('assets/js/main.js'); ?>"></script>
</head>
<body>
<div class="container">
